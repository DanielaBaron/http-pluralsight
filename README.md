<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [HTTP Fundamentals](#markdown-header-http-fundamentals)
  - [HTTP Resources](#markdown-header-http-resources)
    - [Uniform Resource Locators](#markdown-header-uniform-resource-locators)
    - [Ports, Queries, and Fragments](#markdown-header-ports-queries-and-fragments)
    - [URI](#markdown-header-uri)
    - [URL Encoding](#markdown-header-url-encoding)
    - [Content Types](#markdown-header-content-types)
    - [Content Negotiation](#markdown-header-content-negotiation)
  - [HTTP Messages](#markdown-header-http-messages)
    - [Message Types](#markdown-header-message-types)
    - [A Manual Request](#markdown-header-a-manual-request)
    - [HTTP Methods](#markdown-header-http-methods)
    - [Safe Methods](#markdown-header-safe-methods)
    - [GET and POST Scenarios](#markdown-header-get-and-post-scenarios)
    - [Request Messages](#markdown-header-request-messages)
    - [Response Message](#markdown-header-response-message)
    - [Status Codes](#markdown-header-status-codes)
  - [HTTP Connections](#markdown-header-http-connections)
    - [Whirlwind Networking](#markdown-header-whirlwind-networking)
    - [Handshakes with a Shark](#markdown-header-handshakes-with-a-shark)
    - [On the Evolution of HTTP](#markdown-header-on-the-evolution-of-http)
    - [Parallel Connections](#markdown-header-parallel-connections)
    - [Persistent Connections](#markdown-header-persistent-connections)
  - [HTTP Architecture](#markdown-header-http-architecture)
    - [Resources Redux](#markdown-header-resources-redux)
    - [Architectural Qualities](#markdown-header-architectural-qualities)
    - [Adding Value](#markdown-header-adding-value)
    - [Proxies](#markdown-header-proxies)
    - [Proxy Services](#markdown-header-proxy-services)
    - [Caching](#markdown-header-caching)
  - [HTTP Security](#markdown-header-http-security)
    - [The Stateful Stateless Web](#markdown-header-the-stateful-stateless-web)
    - [Cookies](#markdown-header-cookies)
    - [Tracing Sessions and HttpOnly](#markdown-header-tracing-sessions-and-httponly)
    - [Cookie Paths, Domains, and Persistence](#markdown-header-cookie-paths-domains-and-persistence)
    - [Basic Authentication](#markdown-header-basic-authentication)
    - [Digest Authentication](#markdown-header-digest-authentication)
    - [Forms Authentication](#markdown-header-forms-authentication)
    - [OpenID](#markdown-header-openid)
    - [HTTPS](#markdown-header-https)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# HTTP Fundamentals

> Learning HTTP with Pluralsight [course](https://app.pluralsight.com/library/courses/xhttp-fund/table-of-contents)

## HTTP Resources

HTTP: Hyper Text Transfer Protocol. Protocol for web server to communicate with browser.

### Uniform Resource Locators

When typing `http://www.food.com` into web browser, it makes *http* request to server at `food.com`.

`http://www.food.com` is referred to as a *URL*, Uniform Resource Locator. Represents a specific *resource* on the web.
The URL *locatores* the *resource* that is the homepage of the `food.com` website.

A *resource* is something you can interact with on the web, eg: images, pages, files, videos, etc. There are billions, trillions of resources on the web. Each one has a URL to find it. Can have different resources and urls within the same website.

**URL Parts**

![URL Parts](images/url-parts.png "URL Parts")

*URL Scheme* (eg: http, https, ftp, mailto)

How to access the resource. Note that url's can be used for other protocols besides http like ftp, and mail.
Everything after the "://" will be specific to the protocol, so a valid http url may not be a valid ftp url.


*Host* (eg: food.com)

Specifies which computer on the internet is hosting the resource. Requesting computer will use DNS (domain name system) to lookup the hostname (`food.com` in this example), convert that into a network address and send the request there. For example:

Q: Where is food.com?
A: It's at IP address 204.78.50.82

The URL could also contain the IP address instead of the hostname.

*URL Path* (eg: recipe/grilled-cauliflower)

The food.com host will recognize what resource is being requested by the path and respond appropriately.

Paths look hierarchical, like a file system path. Sometimes url will point to real resource that is on host's file system or hard drive.

Resources can also be *dynamic*, i.e. an application is running on the host (eg: php, rails, asp.net etc.) that takes the request and builds a resource using content from a database and then responds to the request with that dynamically built resource, which is HTML that a browser can display.

Some resources will lead browser to download additional resources. For example, first resource fetched may include images, javascript files, css stylesheets etc. i.e. browser will make multiple HTTP requests to retrieve all the resources required to display the page.

### Ports, Queries, and Fragments

**Ports**

`http://food.com:80/recipes/squash`

`80` is the port number host uses to listen for http requests. Default port for http is 80 so it can be ommitted from the url. But if server is listening on a different port (eg: 8080, typically used for development), then it must be specified as follows:

`http://localhost:8080/recipes/squash`

**Queries**

`http://bing.com/search?q=jaboticaba`

Everything after the question mark `q=jaboticaba` is the *query string*. Contains information for the host web site to interpret to determine what resource is being requested. There is no formal standard for how the query string should be formatted, but typically used to pass name/value pairs.

In the example below, the query string has two parameters. The ampersand "&" is used to delimit parameters:

`http://hr-intranet/search?firstName=Scott&lastName=Allenn`

**Fragments**

`http://wikipedia.org/wiki/Jabuticaba#Description`

Everything after the hash "#" is the *fragment*. Unlike path and query string, fragment is ***NOT*** processed by the server. Fragment is *only* used on the client, it identifies a particular section of a resource that the client should navigate to or focus on.

**All Parts**

Example query with all parts described above:

`http://host:8080/path?q=query#fragment`

### URI

Uniform Resource Identifier. More generic than URL. Can identify a resource by either name or location. URL is a type of URI.

### URL Encoding

Standards to ensure url's are as useable and interoperable as possible.

*Unsafe characters* are characters that should not appear in a url. For example, space character, pound sign (because its reserved for delimiting fragment).

[RFC-3986](https://www.ietf.org/rfc/rfc3986.txt) defines safe characters as printable upper and lower case ascii characters, numerals and a few special characters: dollar, dash, underscore, period, plus, star, single quote, open and closing round brackets and comma.

To transmit unsafe characters in a url, they must be *percent or url encoded*. For example:

![URL Encoding](images/url-encode.png "URL Encoding")

### Content Types

Typically when a url is entered into the browser, it indicates that user would like to *retrieve* (i.e. view or read) a resource such as a web page or an image. There are many different *types* of resources that can be retrieved (eg: hypertext docs, images, xml docs, video files, audio files, executable applications, pdf documents, word documents, etc.)

In order for host to property *serve* a resource, and in order for client to property *display* the resource, the type of resource being requested/received must be specified. When host returns a resource, it specifies the *Content Type*, aka Media Type of the resource.

Content types specified by server rely on *MIME Types* (multi-purpose internet mail extensions). Originally designed for email communications, also used by http to label the content type, so client will know what it is. Common types include:

![Common Mime Types](images/mime-types.png "Common Mime Types")

For example, `text/html`, `text` is the *primary* media type, `html` is the *subtype*.

If the appropriate mime types are not registered on the server, then when the client makes a request for say, a pdf resource, if on the server its mime type is registered as `text/html`, then client will receive some text representation of the pdf rather than seeing the pdf document as expected.

### Content Negotiation

HTTP is commonly thought of to serve web pages and images, but its a more generic protocol for moving information around in an interoperable way.

Media types aren't just for hosts, clients can also play a role in the media type returned by server by participating in *Content Type Negotiation*.

A resource identified by a *single* url can have *multiple* representations. For example, a recipe website may offer recipes in different languages (English, French, etc.) or formats (HTML, PDF, plain text, etc.). So when client requests a resource, which representation should server respond with? Answer lies in Content Negotiation as described by the HTTP spec.

When client makes request, it specifies which media types it will accept in outgoing request message. For example it can request a recipe in HTML and French. Server may have it in HTML but only English, or it may have French but only PDF. It's a *negotiation* not an ultimatum.

Useful for web development, for example a JavaScript client can make a request to server for a particular resource, specifying json as the desired content type, whereas a C# client could request the same resource with xml content type.

## HTTP Messages

HTTP 1.1 specification defines the language of requests and responses so that clients and servers can understand each other. Defines what the messages that are exchanged should look like.

### Message Types

HTTP is a request/response protocol, there are two types of messages:

**HTTP Request** Message sent by client to server, formatted such that server can understand it.

**HTTP Response** Message sent by server to client, formatted such that client can understand it.

HTTP Request and Response are two different message types but they are exchanged within a single HTTP Transaction.

When resource does not exist, response message will contain an error message so that client can understand what happened.

### A Manual Request

Web browser knows how to send HTTP Request by opening a network connection to a server, and sending a request message.

Message is a command in plain ascii text, formatted according to the http spec. Any application that can send data over the network can make an http request.

Sample manually constructed request:

```
GET /odetocode.jpg HTTP/1.1
Host: www.odetocode.com
```

`GET /odetocode.jpg HTTP/1.1` specifies HTTP method, resource, protocol, version

`Host: www.odetocode.com` specifies hostname, because server could be running multiple websites.

Sample response:

```
HTTP/1.1 301 Moved Permanently
Location: http://odetocode.com/odetocode.jpg
Server: Microsoft-IIS/7.0
X-Powered-By: ASP.NET
Date: Sun, 15 Jan 2012 16:11:02 GMT
Connection: close
Content-Length: 0
```

This means:

Resource exists but has been moved to another location. It's now at odetocode.com rather than www.odetocode.com.

Web browser parses the response, and recognizes the redirect to issue another request to the correct Location specified in the response.

These types of redirects are common, to ensure all requests for a particular resource go through a single url. This is an SEO technique "URL canonicalization".

If make the manual request again, this time to the correct resource url:

```
GET /odetocode.jpg HTTP/1.1
Host: odetocode.com
```

Response indicates response is successful:

```
HTTP/1.1 200 OK
Content-Type: image/jpeg
Last-Modified: Sat, 03 Oct 2009 20:09:10 GMT
Accept-Ranges: bytes
ETag: "0f76b5e6544caal:0"
Server: Microsoft-IIS/7.0
X-Powered-By: ASP.NET
Date: Sun, 15 Jan 2012 16:14:20 GMT
Connection: close
Content-Length: 11751

...binary representation of image...
```

Headers contain additional information such as when resource was last modified, which client can use for caching.

### HTTP Methods

Every HTTP Request message must include one of the available HTTP Methods such as `GET`.

Method tells the server what the request wants to do. `GET` wants to retrieve a resource.

Common HTTP Methods:

![Common HTTP Methods](images/http-methods.png "Common HTTP Methods")

Web browser sends POST request when it has data it wants to send to server, for example user clicking Add to Cart on e-commerce shopping site. POST requests typically generated by a form element on a web page.

### Safe Methods

Safe methods allow you to read and view resources from a web server. Unsafe methods allow you to change resources on a web server.

`GET` is safe because it only retrieves a resource, but does not alter the state of that resource. i.e `GET` should never have a side effect on the server.

`POST` is unsafe because it typically changes something on the server. Eg: process a credit card transaction, submit an order.

### GET and POST Scenarios

Web browsers treat GET and POST differently due to safety issues. GET's can be issued repeatedly (eg: user refreshing the browser), but for a POST, browser will display warning (could for example end up submitting payment information twice).

Submitting POST from form element causes input values to be placed in http message body, not in the url. Whereas submitting GET from form element converts input values into a query string.

### Request Messages

A full request message consists of a start line, followed by one or more headers, and then an optional body, typically populated for a POST but not GET. Host header is required, but can be many more.

```
[method] [URL] [version]
[headers]
[body]
```

Request headers contain useful information to help server process request. For example, to specify French representation:

```
GET http://server.com/articles/741.aspx HTTP/1.1
HOST: odetocode.com
Accept-Language: fr-FR
Date: Fri, 9 Aug 2002 21:12:00 GMT
```

`Date` header appears in request and response. All but host header is optional, but when present, must follow standards RFC822.

Common request headers:

![Common Request headers](images/request-headers.png "Common Request Headers ")

Sample full request, note some headers can accept multiple values, comma separated:

```
GET / HTTP/1.1
Host: server.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) Chrome/16.0.912.75 Safari/535.7
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Referer: http://www.google.com/url&q=broccoli
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q-0.3
```

`q` is number from 0 - 1, default is 1.0. Represents quality value, relative degree of preference when there are multiple values in a header. In the example above client would absolutely prefer ISO-8859 (100%), then 70% prefer UTF-8, finally all else is 30% like.

### Response Message

Also has a start line (reason is textual description of status), followed by headers, followed by response body:

```
[version] [status] [reason]
[headers]
[body]
```

Sample response:

```
HTTP/1.1 200 OK
Cache-Control: private
Content-Type: text/html; charset=UTF-8
Server: Microsoft-IIS/7.0
X-AspNet-Version: 2.0.50727
X-Powered-By: ASP.NET
Date: Sat, 14 Feb 2003 04:00:08 GMT
Content-Length: 17151

<html>
  ... content ...
</html>
```

`200` indicates HTTP transaction was successful.

`Cache-Control` describes how this response can be cached. `private` means its a private response for the individual user so its ok for the browser to cache the response, but if there's any hardware between server and client, it should not try to cache this for multiple users.

`Content-Type` describes the MIME type of the body and what character set its encoded with.

`Content-Length` so client can tell when it has received the end of this message.

Headers starting with `X-` are *extensions*, reserved for non standard headers.

### Status Codes

Status code tells client the result of the response, whether it was successful or not. All standard status codes fall into one of these ranges:

![Status Code Categories](images/status-code-categories.png "Status Code Categories")

![Common Status codes](images/common-status-codes.png "Common Status Codes")

## HTTP Connections

How do messages actually move through network? When are network connections opened? closed?

### Whirlwind Networking

Network communication protocols (things that move information around internet) consist of *layers*. Each layer in communication stack has a specific responsibility.

![Network layers](images/network-layers.png "Network layers")

**Application Layer**

HTTP is *application layer* protocol because it allows two applications to communicate over the network. Usually one app is a web browser, and the other is a web server. But this layer does not specify *how* messages move across the network, that's the responsibility of a lower layer protocol.

**Transport Layer**

Layer under HTTP is *transport layer*. All HTTP traffic travels over *TCP* (transmission control protocol). When user enters a url in the browser, browser first extracts hostname and port number if present from url, and then opens a *TCP socket* by specifying server's address and port (default 80). Then the browser writes data (i.e. HTTP message) into the socket.

TCP layer accepts data and ensures that it gets delivered to server without getting lost or duplicated. TCP will resend any info that might get lost in transit. TCP also provides flow control, ensures sender does not send data too fast for receiver to process it.

TCP provides services for successful delivery of HTTP messages, in a transparent way. Apps don't need to worry about it, just open a socket and write data into it. Responsible for error detection, flow control, overall reliability.

TCP (Transmission Control Protocol) === Reliable
UDP (User Datagram Protocol) === Unreliable  

**Network Layer**

IP (Internet Protocol) - responsible for taking pieces of information and moving them through switches, routers, gateways, repeaters from one network to the next. Does not *guarantee* delivery of data to next destination. (TCP guarantees it).

IP requires that each computer has an address (i.e. IP address). Responsible for breaking data into packets (aka datagrams). Fragments and re-assembles packets to optimize for some network segments.

**Data Link Layer**

Application, Transport and Network protocols are implemented inside a computer. Eventually those packets have to travel over a wire or fibre optic or wireless link. Common choice is Ethernet. IP packets become *frames*, deals with 1's, 0's, electrical signals.

Eventually signal reaches server, comes in through network card and process is reversed:

* Data link layer delivers packet to IP layer
* IP layer hands the packet(s) to TCP for re-assembly into original HTTP message sent by client
* TCP layer pushes it into the web server process

### Handshakes with a Shark

Wireshark can examine all information flowing through network such as the *TCP Handshake* - the messages required to establish a connection between the client and the server. This happens before the actual HTTP messages start to flow. Can also observe TCP and IP headers, which add 20 bytes each on top of every message.

Start Wireshark and enter a capture filter, for example:

```
host css-tricks.com
```

Select network interface to capture (eg: wifi), then use browser to visit that site and observe packets captured in wireshark.

Each packet displays the Source and Destination ip addresses and Protocol (captures TCP and HTTP).

TCP Handshake is three steps, to make sure client and server are in agreement about how to communicate.

HTTP relies heavily on TCP to take care of all the hard work. And TCP involves some overhead (handshake)

### On the Evolution of HTTP

In the early days of the web, most resources were textual. Typical usage was user would request a web page that was mostly text, server would send it, user would read, and maybe a few minutes later click a link on that page. So it was very easy for computers to open and close TCP connections for every request user made because they didn't happen that frequently. Process for each connection was:

* create new socket
* connect it
* send request
* close socket

But now all web pages require more than a single resource to render (images, css, js, etc.). One web page could spawn 50 or even 100 subsequent requests to retrieve all the resources it needs to render. Would be too slow to open a TCP connection one at a time and wait for each resource to download sequentially before starting the next one. Would be slow due to latency (signals travelling long distances, winding through hardware), plus overhead in establishing a connection (3-step TCP handshake).

### Parallel Connections

Browsers do not make requests serially (one at a time). They open multiple parallel connections to server. For example when downloading html for a page, if browser sees two image tags, it can open two connections to download those images simultaneously.

Max number of parallel connections depends on browser and configuration. Spec actually says "a single-user client should not maintain more than 2 connections with any server", i.e. 2 per hostname. Some workarounds are to use subdomains for assets, for example html could be at `www.odetocode.com` and images at `images.odetocode.com`. Then browser could open 2 connections to each for total of 4 simultaneous connections.

Parallel connections obey law of diminishing returns, 4 may be better than 2, but not 100. Too many can saturate and congest the network. Server can only accept a finite number of connections.

### Persistent Connections

To reduce overhead of establishing connections and improve performance, use *persistent connections*, which are the default type of connection in HTTP 1.1.

Persistent connection stays open after completion of one request/response transaction. Browser is left with already open socket that can be used to continue making requests to server, without overhead of opening a new socket. This reduces memory and cpu usage and improve response time of page.

Persistent connections avoid [TCP Slow Start](https://tools.ietf.org/html/rfc2001) strategy for congestion control.

However, server can only support finite number of connections (depends on memory, cpu, configuration). Can leave server vulnerable to DDoS attacks. Server configured to close persistent connection if its idle for some period of time.

Some servers are configured to not enable persistent connections. In this case, server must send `Connection: Closed` header with each response to signal to the client that the connection will not be persistent. Then client knows they must immediately close the socket after receiving the data, i.e. they are *not* allowed to re-use the same socket to send another request.

If `Connection: Closed` header is not in the response, then server does allow persistent connections.

**Pipeline Connection**

Another optimization. User agent (eg: browser) can send multiple HTTP requests on a single connection, and send them before waiting for the first response. Pipelining allows more efficient packing of requests.

[Why not use it](http://stackoverflow.com/questions/14810890/what-are-the-disadvantages-of-using-http-pipelining).

## HTTP Architecture

### Resources Redux

Resources are the centerpiece of HTTP. Resource abstraction is so much more than just a file on a web server. Resource examples that could be exposed by an application:

* a recipe for beef wellington
* search results for 'deep dish pizza'
* patient 123's medical history

These resources are significant enough to be *identified* and *named*. If a resource can be identified, then it can be assigned a url to locate this resource, for example `http://maps.google.com/maps?q=deep+dish+pizza`. This url can be handed to someone else, for example embedded in hyperlink or sent via email.

URL cannot restrict client or server to a particular language or technology. Everyone speaks HTTP, client could be written in Ruby and server in C++.

URL cannot force the server to store the resource using any particular technology. Resource could be a document on file system. Or web server could respond to incoming request for resource by building it on the fly, using info stored in files, databases, other web services, etc.

URL cannot specify *representation* of a specific resource (eg: could have different representations of same resource like HTML, JSON, XML, etc.). Client can specify representation they want using `Accept` header, for example: `Accept: application/json`. Content negotiation happens via request header, not in url itself.

URL cannot specify what user wants to do witha resource. For example retrieve (GET) or edit (POST, PUT). That's specified as part of the HTTP request message, to describe the *intention* of the requester.

The web (via resources) becomes part of the application, an *architectural layer*. Further reading on REST (representation state transfer style architecture) [Architectural Styles and the Design of Network-based Softare Architectures](http://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf).

### Architectural Qualities

So what can a url do (with http). Benefits of REST style include scalability, simplicity, reliability and loose coupling.

Think of URL as pointer or unit of indirection between client and server. Client specifies their intention via an HTTP message, which is simple, plain text. Request and response messages are standardized and easy to parse.

Request message includes:

* HTTP method (i.e what client wants to do).
* Path to resource.
* Headers to specify (among other things), what representation client wants.

```
GET /maps?q=deep+dish+pizza HTTP/1.1
Host: maps.google.com
Accept-Language: fr-FR
Date: Fri, 9 Jul 2012 12:59:59 GMT
```

Response message includes:

* Status code to indicate result of transaction
* Headers for cache instructions, content type of resource and other metadata about the response.

```
HTTP/1.1 200 OK
Date: Fri, 10 Jul 2012 12:59:59 GMT
Expires: -1
Cache-Control: private, max-age=0
Content-Tpe: text/html; charset=UTF-8Content-Encoding: gzip
Content-Length: 68927
```

ALL information required by transaction is contained within the request/response messages which are both visible easy to parse. HTTP applications can rely on services that provide value as message moves between client and server.

### Adding Value

HTTP message moves from memory space of process on one machine to memory space of process on another machine. Moves through various pieces of software and hardware across the internet that may inspect and modify the message.

Web server process is first recipient of incoming message on a server machine. Web server is responsible for routing messages to appropriate application. For example web server could be hosting multiple apps written in different languages. It inspects the Host header of incoming messages to determine where to direct it.

Web servers can perform other services like logging each incoming message. Can also modify outgoing response messages. For example server knows if client supports gzip compression (because client would have sent that information via Accept encoding header), if yes, then it can compress the response.

Compression and logging are value added service that application doesn't need to worry about.

### Proxies

Proxy server sits between a client and server, usually transparent to client. Proxy server can forward client request messages to appropriate server to handle them. Proxy server can also wait on response from server and forward it to client. Before forwarding request/response, proxy server can inspect message and take some action.

**Forward Proxy**

Usually closer on network to client than server. Usually require some configuration on each client's browser to work. Provides benefits to just some users at a particular location, rather than the Internet wide.

![Forward Proxy](images/forward-proxy.png "Forward Proxy")

For example, proxy server can function as an access control device, a company can forward all their employees http traffic through proxy server, and drop any messages to unapproved hosts, for example to block employees from accessing twitter or facebook during business hours.

Proxy server can also modify the messages, for example to strip out confidential data like the `Referer` header. Can log every request to create an audit trail. Some proxy servers require user to log in before they can get access to the internet.

**Reverse Proxy**

Usually closer to server than client, fully transparent to client. Provides benefit to a particular website.

![Reverse Proxy](images/reverse-proxy.png "Reverse Proxy")

### Proxy Services

Proxy servers can perform many services. For example *load balancing*. Proxy takes incoming messages and distributes them to a collection of web servers based on which server is currently least busy processing requests.

Can direct request to appropriate server depending on content type, for example images may be fulfilled by a dedicated assets server.

Can be used for [SSL Acceleration](https://en.wikipedia.org/wiki/SSL_acceleration), proxy server performs the encryption and decryption of http messages, taking load off web servers.

Can add additional layer of security by filtering out dangerous messages such as those that may contain XSS or SQL injection.

Caching proxies store copies of frequently accessed resources, and respond to messages requesting those resources directly, improving performance.

Proxies do not have to be a separate piece of hardware, could be any software capable of intercepting http requests (eg: Fiddler).

### Caching

Caching is optimization to improve performance and scalability. Multiple requests for the same resource representation, proxy server can cache the representation locally and send it back, rather than have the web server have to generate it over and over again. This reduces the time and bandwidth required for full retrieval. Caching reduces latency, prevents bottlenecks, and can help server support higher loads. Types of caches:

**Public Cache**

Cache shared among multiple users, generally resides on proxy server. Forward proxy caches resources that are popular among community of users using this proxy to access the Internet.

Public cache on reverse proxy is caching resources that are popular on a specific website, eg: popular images on amazon.com.

**Private Cache**

Dedicated to a single user. For example, web browsers keep user's private cache of browsing history in files on the local disk. Enter [chrome://cache/](chrome://cache/) to see list of cached files.

If browser has cached a resource in the file system, it can load almost instantly, doesn't need to send http request to server for that resource.

Rules about what to cache, when to cache, when to invalidate cache (remove a resource when it becomes stale) are complicated.

Clients and proxies generally want to cache any response to a `GET` request that is `200 OK`.

App/server can influence cache setting with appropriate `Cache-Control` header and sometimes `Expires` and `Pragma` header (deprecated):

```
HTTP/1.1 200 OK
Cache-Control: private,max-age=0
Expires: ...
Pragma: ...
Content-Type: text/html; charset=UTF-8
Content-Encoding: gzip
Content-Length: 68927
```

Possible values for Cache Control:

![Cache Control](images/cache-control.png "Cache Control")

`public` means public proxy servers can cache the response, i.e. anyone can be served this cached response.

`private` response is targeted to a single user, only private caches should keep this (i.e. web browser).

`no-cache` no one should cache this, proxy server or browser.

`no-store` message might contain sensitive information that should not be persisted, should be removed from memory ASAP.

**Usage**

Popular requests for public websites like a homepage logo - use `public` cache.

Rendered homepage for logged in user that displays logged in user name - use `private` cache.

Given that something is cached, use `max-age` (specified in seconds) to set how long that cache should last. Use smaller time for things that are expected to change, longer times if a resource, such as an image is not expected to change. For example to set cache expiry of 1 years:

```
HTTP/1.1 200 OK
Content-Type: image/png
Last-Modified: Tue, 23 Aug 2011 12:28:21 GMT
Cache-Control: private, max-age=315360000
```

Browser uses `Last-Modified` header to validate if entry in cache is still good. Browser can send request for resource to server passing `If-Modified-Since` header (with value from `Last-Modified` server response), i.e asking server "here's last date I have for this resource, has it changed since then?". If it has changed, server sends newer resource, otherwise server can send `304` response which indicates this content was not modified, what you have in cache is good.

`ETag` header is another type of cache validator. Typically is a hash of the resource and is used for comparison. Browser can ask server if it still has that resource by sending request header `If-None-Match` with value it got from `ETag` in server response. Server can look at that ETag and compare to current ETag and answer yes its changed or no change (`304 Not Modified`).

Every user agent has heuristics to determine if it should ask server if cached value has changed, or simply serve up the resource from the cache.

## HTTP Security

### The Stateful Stateless Web

HTTP is designed as *stateless* protocol. Each request/response transaction is independent of any previous and future transactions. Protocol does not specify that server should retain state or information about a single http request, it only states that server must generate a response for that request. Every request contains all the information the server needs to create the response.

Being stateless allows for layered services like caching, because every message is self descriptive.

However, most applications built on top of HTTP are *stateful*. For example, banking app forces user to log in first to see their account info. So every time a stateless request for account info arrives at the bank web server, it needs to know more information about the user, i.e. "is this an authenticated user?". If not, user must be redirected to login page.

Another example, opening a new account requires going through a 4 step wizard where each step submits a separate request to server. Server must be able to verify for each step, that user has completed the previous steps.

**Saving State**

Hidden input fields, saving to database on server.

Session on server. Data stored in session is scoped to individual users browsing session (not shared among multiple users). Good for short lived state, discarded when user leaves site or closes browser. But how can user determine given multiple requests, if they're from the same or different users?

### Cookies

For web server to identify and track users. Cookie can be set via `Set-Cookie` header in http response from server. Browser will then set that cookie in the header of every subsequent request sent to that server. So server can set a unique id as cookie value and use that to identify its the same user across multiple requests.

Cookie can *identify* a user, i.e. Bob's cookie will have a different value than Alice's cookie; but they cannot *authenticated* a user. An authenticated user has proven their identity, for example by providing username and password credentials. Example flow:

**Step 1: Request to server**

```
GET /search?q=lyrics HTTP/1.1
Host: searchengine.com
```
**Step 2: Server responds with Set-Cookie header**

```
HTTP/1.1 200 OK
Set-Cookie: SessionID=00a48b7f6a4946a8ad...;
            domain=.searchengine.com;
            path=/
```

**Step 3: Subsequent requests contain Cookie header**

```
GET /search?q=lyrics HTTP/1.1
Host: searchengine.com
Cookie: SessionID=00a48b7f6a4946a8ad...;
...
```

**Step 4: Server processes subsequent request**

Server uses `Cookie` value to lookup that user (from memory, database, cache etc).

Separate pieces of information in a cookie delimited by `;`. Series of name/value pairs delimited by `&`, similar to query format in url, for example `fname=Scott&lname=Allen`.

Server can put anything in a cookie, but common usage is to only place unique id. (max cookie size ~4K).

Web server frameworks can be configured to handle cookies automatically and lookup session state.

### Tracing Sessions and HttpOnly

Note that cookies are set in a particular browser. So if same user opens a new browser window, the cookie will not be there.

Note use of large random numbers for SessionID rather than simple increment, to prevent people from guessing another user's session id and hijacking that user's session by sending an HTTP request with the guessed SessionID.

But someone sniffing network could still see SessionID in request header, only solution is to use https (discussed later in course).

**HttpOnly**

Flag to prevent XSS attack against cookie -  malicious javascript injected into website that can inspect and steal cookie info. HttpOnly flag tells browser to not allow client side script code to access the cookie. It can only be used to populate the `Cookie` http request header.

```
HTTP/1.1 200 OK
Set-Cookie: SessionId=u3y1zctnrr0iamqhnitqSw23; path=/; HttpOnly
...
```

### Cookie Paths, Domains, and Persistence

Cookies shown in previous section were *session cookies*, (different from session data on server) exists only for a single user session, and destroyed when user closes browser.

**Persistent Cookie**

Can outlive browsing session, user agent saves it to disk. So its still there even after shutting down browser and computer.

Distinguishing feature of session vs persistent cookies is persistent cookie needs `expires` value:

```
HTTP/1.1 200 OK
Set-Cookie: someVal=whatever...;
            domain=.searchengine.com;
            path=/
            expires=Monday, 09-July-2012 21:12:00 GMT
```

**Domain**

Once cookie is set, value will be sent to server on each subsequent request, but not all cookies travel to all websites. User agent should only send cookies to site it received that cookie from.

Web site can change this behaviour (somewhat) by restricting the host/domain/path cookie is sent to using `domain` and `path` attributes of cookie.

`domain` attribute allows cookie to span subdomains. For example, if cookie set from `www.searchengine.com`, browser will only send that cookie to exactly `www.searchengine.com`. But if cookie domain is set to `.searchengine.com`, cookie will be sent to any url in `searchengine.com` domain such as `images.searchengine.com`, `help.searchengine.com` etc.

`domain` attribute cannot be used to span domains, for example, cookie with domain of `.searchengine.com` cannot be sent to web site `microsoft.com`.

`path` attribute used to restrict cookie to particular resource path. For example, given `domain=.searchengine.com;` and `path=/`, cookie will be sent to ANY resource under `.searchengine.com`. But if had `path=/images`, then cookie would only be sent for requests where url path starts with `/images`.

### Basic Authentication

HTTP supports different authentication models. This section discussed *basic auth*, which is part of the HTTP spec.

When need to know an individual user's *identity* - who they are. Authentication is the process by which user proves their identity by entering credentials such as username and password.

On the web, authentication follows challenge/response format. Client requests a secure resource:

```
GET /account HTTP/1.1
Host: giantbank.com
```

Server responds with a challenge for user to authenticate. In this example, `Basic` authentication protocol is specified in response. `WWW-Authenticate` tells user agent to collect user's credentials and try again. `Basic realm` gives user agent a text description of the protected area.

```
HTTP/1.1 401 Unauthorized
WWW-Authenticate: Basic realm="Giant Bank"
```

Client must send another request including authentication credentials for server to validate. If credentials are good, request succeeds.

Browser will popup a dialog asking for username and password, then will send another request with `Authorization` header:

```
GET /account HTTP/1.1
Host: giantbank.com
Authorization: Basic Z2VudDptaXNzaW5n
```

Value of `Authorization` header is username and password Base64 encoded. i.e. this is INSECURE because anyone scanning the network traffic can decode this value and get username and password. For example `Z2VudDptaXNzaW5n` run through Base64 decoder might be `jdoe:mypassword`. Basic auth rarely used without https.

Now server decodes Authorization header, verifies username/pswd (by checking OS, db, whatever credential mgmt system is configured on server). If credentials match, server responds with requested resource (`/account` in this example). Otherwise server replies with `401 Unauthorized`.

All subsequent requests will be sent with same Authorization header.

### Digest Authentication

Also part of HTTP spec. Some improvement over basic auth because doesn't transmit user passwords with base64 encoding. Client sends *digest* of password, which client computes using MD5 hashing algorithm with *nonce* provided by server during authentication challenge, which prevents replay attacks.

```
GET /account HTTP/1.1
Host: giantbank.com
...
```

```
HTTP/1.1 401 Unauthorized
WWW-Authenticate: Digest realm="Giant Bank",
                  qop="auth,auth-int",
                  nonce="dcd98710...00bfb0c093",
                  opaque="5ccc069....e9517f40e41"
```

Rest of process is the same, except next client request will submit *encrypted* credentials.

Digest auth still vulnerable to *man in the middle* attacks.

### Forms Authentication

Not part of HTTP spec. Gives application complete control over the login experience.

Client makes request for secure resource:

```
GET /account HTTP/1.1
Host: giantbank.com
...
```

Server responds by redirecting user to login page using a temporary redirect. Url that user was originally requesting may be included in the query string of the redirect location:

```
HTTP/1.1 302 Found
Location: /login?ReturnUrl=/account
```

User will typically be redirected to a page that contains a form:

```html
<form method="post">
  <input type="text" name="username">
  <input type="password" name="password">
  <input type="submit" value="Login">
</form>
```

When user clicks `Login` button, will submit POST operation to login destination. App validates credentials against credential mgmt system.

Credentials are submitted in plain text, not secure unless using https.

Upon successful login, server will send a redirecto to location user was originally trying to access (eg: `/account`) and set a cookie indicating user is authenticated. Cookie value should be encrypted and hashed to prevent tampering. Again, without https cookie is vulnerable to being intercepted.

### OpenID

For cases where applications don't want the complete control, responsibility and risks of forms authentication. For example, don't want to store user passwords. Also users may not want to have to create a new username and password on every site they use.

OpenID solves these problems - open standard for decentralized authentication. User registers with an OpenID identity provider, and only this provider stores and validates credentials. Some providers include Google, Yahoo, Verisign.

When an app needs to authenticate a user (eg: StackOverflow), it works with user and identity provider. User verifies their username/password with identity provider. App will find out if that was successful via cryptographic tokens/secrets that are exchanged.

### HTTPS

Secure HTTP. Encrypts http messages before they travel across the network.

`https` scheme is used in the url, for example `https://github.com`. Default port for http is 80, and for https 443. Browser will connect to appropriate port depending on the scheme.

Works by adding additional security layer in network protocol stack, called Transport Layer Security (TLS), between Application and Transport layers.

![HTTPS](images/https.png "HTTPS")

Message is encrypted before reaching TCP layer. It can only be decrypted by recipient server.

Requires server to have a cryptographic certificate, which is sent to client during https setup of communication channel. Certificate includes the hostname.

Browser uses this certificate to validate its actually talking to the server that it thinks its talking to. Validation is possible via public key cryptography and certificate authorities (eg: Verisign) that sign and vouch for integrity of the certificate.

Administrators purchase and install certificates from certificate authorities (or free from [EFF](https://www.eff.org/)) and install cert on web server.

All traffic over https is encrypted, i.e. http request and response messages, including http headers, message body, cookies, url path, url query string. Everything is encrypted except hostname.

Prevents session hijacking because no evesdroppers can inspect message and steal cookies. Server is authenticated to client via server's certificate. i.e. if browser is sending messages to `mybank.com`, can be guaranteed they are really going to `mybank.com` and not some proxy server that is placed to intercept requests and spoof response traffic from `mybank.com`.

Does not authenticate client, apps still need to implement an auth protocol such as forms auth. Does make forms auth more secure because all data sent back and forth including cookies are encrypted.

Can also use client side certificates with https, which authenticate client in most secure manner. But not generally used on open internet since most users will not purchase and install a personal certificate. Used by some enterprises that require a client cert on each machine of their employees. In this case corporation acts as a certificate authority and issues each employee their own cert.

**Downsides**

Computationally expensive therefore can slow down performance.

Impossible to cache http traffic in public cache due to encryption (although user agents may keep private cache).

Connections are expensive to setup and require additional handshakes between client and server to exchange cryptographic keys and ensure they are communicating with secure protocol. Persistent connections can alleviate some of this. 
